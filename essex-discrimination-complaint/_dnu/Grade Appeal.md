# Grade Appeal

## ~ Final Grade:
> []() | []()
> -------|-------
> Date Assigned: | January 11, 2018
> Assignment Type: | Final Grade
> Percentage of Final Grade: | 100%
> Grade Received: | F
> Action Requested: | Transcript Expungement
> Actionable Grounds: | Religious Discrimination, Neglect, Poor Quality, 

#### ~ My Appeal:

-  I experienced religious discrimination during class.

-  The professor ignored emails I sent to him with my work attached, including multiple follow-up emails from me about the subject.  

-  The course I took was not a serious approach to History 101.

_____

## ~ Assignment #01:
> []() | []()
> -------|-------
> Date Assigned: | December 21, 2017
> Assignment Type: | Class Participation
> Percentage of Final Grade: | 1% or less
> Grade Received: | Unknown
> Action Requested: | Transcript Expungement
> Actionable Grounds: | Religious Discrimination

### ~ Assignment Prompt:[^1]
> This is a rank order exercise.  Do not record your name.  There is no right or wrong answer.  Please answer the question honestly; be careful not to allow your neighbor to see your ranking.  Write a 1 next to your number one preference, a 2 next to your second preference, and so forth.  Be sure to rank all choices from first to last.
>
> Imagine you are living with a family of a different culture or religion for a few months.  At meals they say a grace that is affiliated with a culture or religion different from your own.  Would you
>
> | []()          |        |
> |:--------------------:|:----------------- |
> | []() | join in           |
> | []() | sit silently      |
> | []() | try to get them to change the grace to a more universal tone |
>
>Briefly explain your ranking.

#### ~ Nash's In-Class Instructions:

Nash instructed that we were apart in some ways, including religion, and needed to "become one."  Therefore, we were to:

1.  Use the assignment to "create a scenario."
		
1.  Dissolve into groups of four and proselytize to one another.
		
1.  Reach a consensus on "how to rank the items."

#### ~ My In-Class Rebuttals to Nash:
After receipt of the assignment:

1.  I asked if I could leave.

1.  Nash told me:

	1.  No, I could not.

	1.  I needed to "wait" because he was "putting pressure" on his students.

	1.  I could voice my protestation at the end of class.

	1.  Nash addressed the class:

		1.  This was a mandatory exercise, which -- according to him -- would only "work" if each and every student participated.

1.  After having placed us in groups of four, Nash walked among the student groups, listening to our conversations.  While doing so:

	1.  Nash asked my group:

		1.  What consensus we had reached.

	1.  We responded with:

		1.  We had a consensus.

		1.  Our answer included the caveat that no person should ever be forced to pray.

	1.  Nash instructed that we "keep trying."

1.  Near the end of class, Nash opened the floor for student discussion:

	1.  I said:

		1.  I would not engage with someone who was forcing me to pray.

	1.  Nash replied:

		1.  People in "places of authority" were the ones to make "those choices."

		1.  We, his students, were not in "positions of authority."

		1.  A person was not "denied" or "diminished" in any way by the simple act of prayer.

		1.  It was disrespectful for a guest to not pray with their host.

#### ~ My Appeal:

-  The assignment as written treated the subject as private and sensitive.  We know this, because it specifically instructed the students not to record their names. However:

	-  Nash removed all anonymity protections from the assignment.  As a result, he (and everyone else in the class) knew who held which religious beliefs.

	-  Nash personally harassed his students about their personal religious beliefs.

	-  Nash ordered his students, against threat of academic retribution, to harass each other about their personal religious beliefs.

	-  Nash instructed to unapologetically offend one another.
	
-  *Nash violated my First Amendment rights, including those protected by the Establishment Clause of the First Amendment of the U.S. Constitution*

_____

## ~ Assignment #02:
> []() | []()
> -------|-------
> Date Assigned: | December 21, 2017
> Assignment Type: | Essay
> Percentage of Final Grade: | 13.33%
> Grade Received: | B+
> Action Requested: | Transcript Expungement
> Actionable Grounds: | Religious Discrimination

### ~ Assignment Prompt:[^2]
> Referring to the rank order activity we did in class today, write an essay (intro, body, conclusion) defending the position of someone who would sit silently, join in, and use as a teachable moment [sic].  You may be creative and imaginative in this assignment by choosing two or more cultures or civilizations that you have some knowledge about and/or experience with.

#### ~ Nash's Marks:
1.  The following text is from my essay:

	> "All three of these scenarios -- Native American treatment, African Slave Trade, and Opium Wars -- have one common thread:  they each resulted in severe economic gains for the Europeans at the expense of the subjugated communities."

	1.  Nash wrote:

		> "Ok, is war, in your mind only about economics, or can it be about such things as ideas?"

	#### ~ My Appeal:

	- Nash's critique misrepresents my argument.  At no point did I suggest that war was only about Economics.  What I did say, however, was that it was economically beneficial for the oppressors at the expense of the oppressed.

	- Nash did not address my essay in it's entirety.  I later call civilization and religion ideas that were "pushed" within Europe to "justify" their actions.

1.  Nash underlined the following text from my essay: 

	> ". . . prisoners today make little to nothing for their labor."

	####	~ My Appeal:

	- Nash did not explain why he underlined my words.  He did not call my statement false.  It is not a grammatical mistake.  As a personal essay, no citation is required.  Therefore, Nash's mark is not a negative critique.

1.  Nash underlined where I had written

	> ". . . justify . . ."

	After re-inserting surrounding statements from my essay for context, the amended text reads as follows:

	> ". . . This notion of African complicity in the Atlantic slave trade has been used by Europeans to justify the existence of the slave trade itself and as proof as African barbarism \[sic].  As before, Europeans used this as an opportunity to bring religion to a people who, according to them, are \[sic] so uncivilized as to sell their neighbor into slavery."

	1.  Nash wrote:

		> "It has, but there is so much evidence we have to refute those absurd assertions."

	#### ~ My Appeal:

	- Nash is stating the obvious. He is also echoing my own argument, despite having disagreed with it earlier.  Therefore, by definition, Nash's mark is not negative criticism.

1.  Nash underlined where I had written:

	> ". . . even sitting quietly and remaining to oneself can provoke a war."

	After re-inserting surrounding statements from my essay for context, the amended text reads as follows:

	> "Time and time again, the British would sail to China and point their guns inland, trying to force the country to initiate trade.  However, the Chinese didn't want what the Brits were selling, and, as a result, the British (and subsequently other Europeans and Americans) began smuggling opium into China.  Witnessing the addiction and decline of their population, China decided to defend itself and thus began the opium wars \[sic].  Over time, China gave in and signed a treaty allowing at least diplomatic ties with Britain, and soon every other foreign country that came knocking.  When Britian \[sic] decided it wanted even more, it returned with its navy and its weapons.  The lesson to be learned is this:  even sitting quietly and remaining to oneself can provoke a war.  In regards to China, the Brits believed that by forcing China to trade with them, they were bringing them 'civilization,' and pushed this idea within their own homeland."

	1.  Nash then wrote the following:

		> "One should never sit quietly remaining to oneself.  As [sic] our greatest impact is made in community effort."

	#### ~ My Appeal:

	- Nash's criticism misrepresents my argument.

	- At no point did I argue against the benefits of cultural exchange or community effort.

	- Nash's criticism dismisses the evidence.  This is particularly disturbing, given the gravity of the material.

	- Nash's assertion is flawed:

		- There are times when the need for self-defense outweighs the need for "community effort."

1.  Nash underlined where I had written:

	> "The problems begin to arise when one party, whether through greed, malice, or self-righteousness, decides to violate that trust."

	1. Nash commented:

		> "True!"
		
	After re-inserting surrounding statements from my essay for context, the amended text reads as follows:

	> "As we have seen, when it is done correctly with both respect and trust, cultural exchange can benefit both societies involved.  The problems begin to arise when one party, whether through greed, malice, or self-righteousness, decides to violate that trust.  If there is anything to be learned from history, it is that collaboration with foreign nations must always be executed with a level of skepticism and a sense of self-preservation to ensure the continued success of any self-ruling civilization."

	1.  Nash wrote:

		> "Globalization seems to have connected us all in ways we have never imagined it would.  Thoughtful and good historical points. B+"

		#### ~ My Appeal:

		- At first glance, this looks like a positive comment.  However, "Globalization" is not a term that I promote in my essay.  As a concept, it does not support my thesis -- it actually works against it. For some reason, Nash is advocating for it here.

		- Nash calls my historical points "thoughtful" and "good," but his grading is ambiguous.  He earlier dismissed these same "historical points" as irrelevant.

		- Given:

			- The assignment as a personal essay and not a research paper;

			- The writing prompt itself as a response to Assignment #01; and,

			- The parent assignment's religious leanings;

			- Nash's errors in grading my work, such as:

				- Not explaining his markings;

				- Misrepresenting my arguments;

				- Advocating for flawed assertions; and,

				- Rejecting -- then accepting -- both my evidence and subsequent conclusions;

			- Then, clearly:

				- Nash intended I write about my personal religious experiences, including the experience of being proselytized to during class, and graded my work negatively because:

					- I had not; and,
					- I did not present religion in a more positive light;

			- *Nash violated my First Amendment rights, including those protected by the Establishment Clause of the First Amendment of the U.S. Constitution*
_____

## ~ Assignment #03:
> []() | []()
> -------|-------
> Date Assigned: | January 02, 2018
> Assignment Type: | Class Participation
> Percentage of Final Grade: | 1% or less
> Grade Received: | Unknown
> Action Requested: | Transcript Expungement
> Actionable Grounds: | Religious Discrimination

### ~ Assignment Prompt:[^3]

> *1.  "Humankind has an ongoing search to connect with something greater than themselves.  True or False"*
>
> *2.  "What have you experienced that have allowed you to witnessed some of the worlds most fascinating spiritual journeys? \[sic] Do you have a story to share?  Can you tell the story in a page?"*

#### ~ Nash's In-Class Instructions:

1.  Nash instructed us:

	1.  To write our responses into a page;

	1.  To read our responses aloud in front of the class; and,

	1.  That failure to comply would reflect negatively on our grades.

1.  One minute after completing his instructions, Nash walked up to my desk and asked me:   

	> "Anything ever happen to you that you can't explain?"

	1.  I said to him that I was still considering the problem.

	1.  Nash replied:

		> "Hmm."

1.  Nash then walked over to other students' desks, asking them each the same question that he had just asked me.

1.  After I handed in my assignment, Nash redirected the current class activity -- which, at the time, was the playing of multiple episodes of The Oprah Winfrey Show about religion -- to now become the oral presentation of our work.  Before releasing us to begin, however, he declared that:

	1.  We, people of "the modern world" and "Western Civilization" were "arrogant," especially when compared to "The Ancients."

	1.  The Ancients possessed a "sacred," "oral" knowledge that modern society does not understand.

	1.  Atheism and Agnosticism, the "beliefs" of modern society are "in fact" themselves faith-based belief systems.

	1.  Myself an Atheist, I found this statement odd.  I found a response by an organization named American Atheists, (www.atheists.org) and prepended my planned presentation with it.  It was:

		> "Atheism is not an affirmative belief that there is no god nor does it answer any other question about what a person believes.  It is simply a rejection of the assertion that there are gods."

1.  Nash's reply to this, was that their statement was somehow my opinion!

	1.  This accusation was unfounded.  I had not influenced their article in any way.

	1.  Nash was so angry that I read this in class, he held me after class and accused me of bad behavior.

#### ~ Nash's Marks:

1.  In response to the the following text from my written assignment:

	> "Answer: True, but the question is misleading"

	1.  Nash wrote:

		> "Why?"

	Inserting my surrounding written statements for context, we get:

	> "Certain humans, like astronomers, search for larger bodies, therefore "something greater than themselves."  Any other interpretation of the word "greater" requires further definition as the term itself is ambiguous in nature when used outside its literal meaning."
	>
	> "Since there are many humans everywhere who do not subscribe to the belief of "something greater," then it would therefore be wrong to assert that the belief in something greater or the pursuit of something greater is inherent to human physiology, the human condition, or humankind."

	#### ~	My Appeal:

	- Nash asked for a True or False answer, and I gave him one.  The fact that I added an explanation should not warrant a negative mark.

	- I already provided an explanation, so it is unclear as to why he is requesting one.

1.  Next to the following text from my written assignment:

	> "Nothing.  No.  Also no."

	1.  Nash wrote:

		> "I see.  Thanks for thinking about it."

	#### ~ My Appeal:

	- Nash is grading me out of context.  He seems to have written this mark without having read the context below it.  As we will see, this is not all he has to say.

1.  Next to the following text from my written assignment:

	> "There have been no double-blind studies that prove the existence of spirits or ghosts to a reasonable-enough degree as to survive peer review, despite many studies.  One quality tested during peer review includes something called 'repeatability,' meaning that anyone should be able to generate the same results from an experiment if given the same conditions.  We find time and time again that the repeatability test fails when applied to spirits and ghosts."

	1.  Nash wrote:

		> "There have been no studies I know of that prove that \[sic] don't exist either."

	#### ~ My Appeal:

	- The burden of proof is on the person making the assertion.  In this case, that would be Nash, who is advocating for the existence of spirits.

	- I provided Richard Dawkins' Flying Spaghetti Monster Orbiting the Earth as an example of a statement that would require evidence to accept as fact.


#### ~ Nash's In-Class Rebuttals to Me:

1.  Nash debated me persistently, both during and after class, about my personal spiritual beliefs, even after I had already taken my seat and asked him not to.

1.  Nash often interrupted me -- sometimes with a yell -- to silence me during my responses.

1.  Nash repeatedly took my arguments out of context, choosing instead to address ideas of his own manufacturing which resulted from his own flaws in interpreting my statements.  He made up some stuff and then argued with himself.

	1.  This occurred even after I implored him to use my words in their proper context.  Rather than ask me to repeat myself for understanding, he silenced me so that he could continue addressing his manufactured ideas.

	1.  He continued to ask me the same questions, even after I had already answered them.  For example:

		1.  Nash very intently and persistently argued that Atheism is an affirmation belief against the existence of gods, despite my having presented him with evidence to the contrary via the American Atheists' statement.

		1.  Nash responded to scientific evidence by calling it a faith-based "belief that you have!"

		1.  He relied heavily on deceitful debate tactics, such as Pseudo-profundity, Moving the Semantic Goalposts, Going Nuclear, and quite a few others discussed in Stephen Law's book, Believing Bullshit: How Not to Get Sucked Into an Intellectual Black Hole.  In fact, Nash seemed so fluent in these tactics that he often switched back and forth between them, and all of his arguments used them.

	1.  He kept me after class to discipline me, where:

		1.  He accused me of proclaiming that other students didn't have the right believe in religion, something I had not done.

		1.  He used the opportunity to proselytize to me;

		1.  He justified his grading of my work using his written responses to the day's assignment.

		1.  He accused me of not taking the course seriously.

			-  I replied "I love learning," to which Nash responded, "I can tell that."  Therefore, I am at a loss as to why he would believe I did not take my work seriously.

		1.  He insisted that, as long as he uses the words "people say," he can say anything he wants and, as long as someone, somewhere, says it -- even if it isn't true -- "That's' a fact!"

		1.  It appeared he was not going to dismiss me unless I declared that I had been wrong and he had been right.

		1.  Luckily for me, another Atheist student, witnessing this, spoke up in my defense.  He said his entire family, also Atheists, agreed with my position that Atheism was not a belief system.  I used the opportunity to leave Nash's desk and began packing my things.

			-  The other student also said that he noticed quite a few women students in the class who seemed ready to debate with me after my presentation.  The point, he said, was that he understood why I did not want to answer questions about my presentation.

			-  Nash's response to this was that, according to himself, none of the women students in the class -- unlike the three men present-- were intelligent enough to understand anything I had said during my presentation.  Furthermore, Nash continued, this deficit was caused by an innate quality, not the level of their education.

#### ~ My Appeal:

- Nash allowed the oral reading of Biblical Scripture during this assignment.

- The assignment prompt presented "fascinating spiritual journeys" as fact.  If students disagreed with this, Nash harassed them.

- Nash suggested myself and another Atheist student of his visit an Ethical Culture Center, which is a meeting place for followers of the Ethical Culture religion.

- *Nash violated my First Amendment rights, including those protected by the Establishment Clause of the First Amendment of the U.S. Constitution*
_____

###### ~ Footnotes:

[^1]:  *Appendix iii., Assignment #01 References*
[^2]:  *Appendix iv., Assignment #02 References*
[^3]:  *Appendix v., Assignment #03 References*


