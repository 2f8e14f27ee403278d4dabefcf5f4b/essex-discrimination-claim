# Research Paper Proposal:  Paper #03
_____

## Question:  

* "Where did the algorithm come from, and how did it become essential to modern life?"
_____

## Summary Explanation:

* The algorithm plays an important role in modern society.  Every computer runs on algorithms.  Additionally, its underlying math, algebra, is a precursor to another important study of math, namely, Calculus.  I would like to learn what person or culture was responsible for the algorithmís development, so that I may dig deeper to see what else of interest I may find.  While my question is very specific, I suspect that other algebraic concepts and even algebra itself will need to be surveyed for a full understanding of the topic.
_____

## Outline:

1.  Section #1:  Define the algorithm.  Briefly describe its usage historically.  Introduce the main pieces of evidence that we will be considering throughout the essay.

2.  Section #2:  Explore the development of the algorithm.  Consider its precursors. 

3.  Section #3:  Consider the greater context of the algorithm, including math in general.  Use this information to track the evolution of the algorithm over time, beginning from its inception and ending in the present.

4.  Section #4:  Explore any broader themes that may become apparent during my research.  Such topics may include music, art, language, and technology.  Look at the civilization that invented the algorithm, and compare and contrast its situation today with ancient times.
_____

## Sources:

1.  Worlds Together, Worlds Apart, 4th ed (Vol. 1).  Hansen and Curtis.  (c) 2014, W.W. Norton and company.

2.  Voyages in World History (Vol. 1):  To 1600.  Tignor, Adleman, et al. (c) 2010, Wadsworth.

3.  A History of Algorithms:  From the Pebble to the Microchip.  Chabert.  (c) 1999, Springer.
