###### Mykael Alexander
###### Prof. Mikal Nash
###### HST 101 - World Civilizations 1
###### 01/02/2018

_____

## Section 1:

### Prompt:  “Humankind has an ongoing search to connect with something greater than themselves.  True or False”

### Answer: True, but the question is misleading
#### Certain humans, like astronomers, search for larger bodies, therefore “something greater than themselves.”  Any other interpretation of the word “greater” requires further definition as the term itself is ambiguous in nature when used outside its literal meaning.  

#### Since there are many humans everywhere who do not subscribe to the belief of “something greater,” then it would therefore be wrong to assert that the belief in something greater or the pursuit of something greater is inherent to human physiology, the human condition, or humankind.
___

##  Section 2:

### Prompt:  “What have you experienced that have allowed you to witnessed some of the worlds most fascinating spiritual journeys?  Do you have a story to share?  Can you tell the story in a page?”

### Answer:  Nothing.  No.  Also no.

#### There have been no double-blind studies that prove the existence of spirits or ghosts to a reasonable-enough degree as to survive peer review, despite many studies.  One quality tested during peer review includes something called “repeatability,” meaning that anyone should be able to generate the same results from an experiment if given the same conditions.  We find time and time again that the repeatability test fails when applied to spirits and ghosts.


#### What is a human trait, however, is curiosity.  Curiosity of the unknown. That isn’t a spiritual desire.  Its a human one, like lust, greed, jealousy, pride, shock, and yes, even awe. 

#### I will leave you with an excerpt of the Table of Contents from “The Magic of Reality,” by Richard Dawkins.  The list below expresses questions that are perfectly human to ask, and require no spirits to explain:

> 1.  What is reality?  What is Magic?
> 2.  Who was the first person?
> 3.  Why are there so many different kinds of animals?
> 4.  What are things made of?
> 5.  Why do we have night and day, winter and summer?
> 6.  What is the sun?
> 7.  What is a rainbow?
> 8.  When and how did everything begin?
> 9.  Are we alone?
> 10. What is an earthquake?
> 11. What is a miracle?
